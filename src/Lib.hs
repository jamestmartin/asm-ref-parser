-- REFERENCES USED IN WRITING THIS CODE:
--  * http://ref.x86asm.net/index.html
--  * https://wiki.osdev.org/X86-64_Instruction_Encoding

{-# LANGUAGE Arrows, NoMonomorphismRestriction #-}

module Lib
    ( someFunc
    ) where

import Data.List.NonEmpty (NonEmpty, fromList)
import System.IO (FilePath, readFile)
import Text.ParserCombinators.Parsec

type NonEmptyString = NonEmpty Char
type HexBinary = Int
type YesNo = Bool
newtype AtMostThirtyTwoOf a = AtMostThirtyTwoOf [a]
type TwoBits = (Bool, Bool)
type FourBits = (Bool, Bool, Bool, Bool)
type OneOrZero = Bool
type URL = NonEmptyString
type ReferenceVersion = String

atMostThirtyTwoOf :: [a] -> AtMostThirtyTwoOf a
atMostThirtyTwoOf xs | length xs <= 32 = AtMostThirtyTwoOf xs

-- | The data type corresponding with x86Reference.xml.
data X86Reference = X86Reference -- | Instructions with one-byte opcodes.
                                 { one_byte :: Opcodes
                                 -- | Instructions with two-byte opcodes.
                                 , two_byte :: Opcodes
                                 -- | General notes on instructions.
                                 , gen_notes :: GeneralNotes
                                 -- | Notes for the Ring Level, used in case of f mark.
                                 , ring_notes :: RingNotes
                                 -- | The version of this X86 reference xml.
                                 , version  :: Maybe ReferenceVersion
                                 }

skipjunk :: Parser ()
skipjunk = spaces >> try (string "<!--" >> manyTill anyChar (try (string "-->")) >> skipjunk)

parseX86Reference :: Parser X86Reference
parseX86Reference = do
  string "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
  skipjunk
  string "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\""
  skipjunk
  string "xsi:noNamespaceSchemaLocation=\"x86reference.xsd\">"
  skipjunk
  string "<one-byte>"
  skipjunk
  one_bytes <- parseOpcodes
  skipjunk
  string "</one-byte>"
  skipjunk
  string "<two-byte xml:id=\"two-byte\">"
  skipjunk
  two_bytes <- parseOpcodes
  skipjunk
  string "<gen_notes>"
  skipjunk
  gen_notes <- many parseGeneralNote
  skipjunk
  string "</gen_notes>"
  skipjunk
  string "<ring_notes>"
  skipjunk
  ring_notes <- many parseRingNote
  skipjunk
  string "</ring_notes>"
  skipjunk
  string "</x86reference>"
  skipjunk
  eof
  return $ X86Reference one_bytes two_bytes gen_notes ring_notes Nothing

parseOpcodes :: Parser Opcodes
parseOpcodes = undefined

parseQuoted :: Parser String
parseQuoted = do
  char '"'
  manyTill anyChar (try (char '"'))

parseGeneralNote :: Parser GeneralNote
parseGeneralNote = do
  string "<gen_note id="
  note_id <- fromList <$> parseQuoted
  string "/>"
  skipjunk
  return $ GeneralNote note_id

parseRingNote :: Parser RingNote
parseRingNote = do
  string "<ring_note id="
  note_id <- fromList <$> parseQuoted
  string "/>"
  skipjunk
  return $ RingNote note_id

-- | General notes on instructions.
type GeneralNotes = [GeneralNote]

-- | General notes on instructions.
data GeneralNote = GeneralNote { general_note_id :: NonEmptyString }

-- | Notes for the Ring Level, used in case of f mark.
type RingNotes = [RingNote]

-- | Notes for the Ring Level, used in case of f mark.
data RingNote = RingNote { ring_note_id :: NonEmptyString }

-- | A set of instruction opcodes.
type Opcodes = [PrimaryOpcode]

data PrimaryOpcode = PrimaryOpcode
                       -- | The processor with which an instruction was added. See ProcStart for details.
                       { primary_proc_start :: Int
                       -- | The set of specific instructions with this opcode.
                       , opcode_entries     :: [OpcodeEntry]
                       -- | An instruction's primary opcode.
                       -- This is the second opcode in the case of two- and three- byte opcodes.
                       , opcode             :: HexBinary
                       }
  
-- | The specific instruction details of a primary opcode.
data OpcodeEntry =
  OpcodeEntry -- | The fixed extraordinary prefix, which may change the semantic of
              -- the primary opcode. Usually used in the case of waiting x87 FPU
              -- instructions, and many SSE instructions. For example:
              -- F3-90 is PAUSE, 9B-D9/7 is FSTCW, and F3/0F10 is MOVSS.
              { opcode_prefix        :: Maybe HexBinary
                -- | Indicates how the ModR/M byte is used.
              , opcode_modrm_usage     :: ModRMUsage
                -- | A fixed appended value to the primary opcode. It is used in some
                -- special cases, x87 FPU instructions and for new three-byte
                -- instructions. For example, D40A AAM, D50A AAD, D5F8 FLD1,
                -- and three-byte escape 0F38.
              , opcode_secondary    :: Maybe SecondaryOpcode
                -- | The processors with which an instruction is compatible.
              , opcode_proc_compat      :: ProcCompat
                -- | The mnemonics and operands valid for use with this opcode.
              , opcode_syntaxes    :: NonEmpty SyntaxType
                -- | The categorization of this instruction.
              , opcode_group1      :: NonEmptyString
              , opcode_group2      :: Maybe NonEmptyString
              , opcode_group3      :: Maybe NonEmptyString
                -- | The way an instruction interacts with the flag register.
              , opcode_flags       :: InstFlags
                -- | A short, very general description of an opcode.
              , opcode_note        :: Maybe String
              , opcode_doc_part_alias_ref :: Maybe NonEmptyString
              , opcode_part_alias  :: Maybe NonEmptyString
              , opcode_mod         :: Maybe PriOpcdMod
                -- | Designates how an FPU instruction interacts with the register stack.
              , opcode_fpu_stack   :: FPUStack
              , opcode_fields      :: InstrFields
                -- | A reference to the 64-bit document.
              , opcode_doc64_ref   :: Maybe NonEmptyString
              , pri_opcd_alias       :: Maybe NonEmptyString
              , pri_opcd_particular  :: Maybe YesNo
                -- | The degree to which this instruction is documented in the Intel manuals.
              , pri_opcd_doc_status  :: DocumentationStatus
                -- | The ring level an instruction is valid from.
              , opcode_ring        :: RingLevel
                -- | A reference to either the 64-bit or 32-bit document.
              , pri_opcd_ref         :: Maybe NonEmptyString
                -- | The mode of operation for an instruction.
              , pri_opcd_mode        :: OperationMode
                -- | A reference to the 32-bit document.
              , pri_opcd_doc1632_ref :: Maybe NonEmptyString
              , pri_opcd_attr        :: Maybe PriOpcdAttr
                -- | Is this instruction valid with the lock prefix?
              , pri_opcd_lock        :: YesNo
              , pri_opcd_op_size     :: Maybe OneOrZero
              }

data ModRMUsage = ModRMExtension Int | ModRMRMOperand | ModRMNone

modRMExtension :: Int -> ModRMUsage
modRMExtension x | x >= 0 && x < 8 = ModRMExtension x

data SecondaryOpcode = SecondaryOpcode { secondary_opcode        :: HexBinary
                                       , secondary_opcode_escape :: Maybe YesNo
                                       }

-- | The range of processors with which an instruction is compatible.
data ProcCompat = ProcCompat -- | The processor with which an instruction was added.
                             { proc_compat_start :: ProcStart
                             -- | The last processor which supported this instruction.
                             -- | If left empty, this instruction is still forward-compatible.
                             , proc_compat_end   :: Maybe Int
                             -- | The instruction extension group an opcode was released on.
                             , proc_compat_ext   :: Maybe InstrExt
                             }

-- | The processor with which an instruction was added.
-- | For 32-bit instructions, empty means 00+ (since the 8086).
-- | For 64-bit instructions, empty means P4++ (since the Pentium 4).
-- | For a table of these values, see http://ref.x86asm.net/index.html#column_proc                            
data ProcStart = ProcStart { proc_start_content  :: Int
                           -- | This instruction is supported in all later processors *and* 64-bit mode.
                           -- I think? This could probably afford verification. Same with lat_step.
                           , proc_start_post     :: Maybe YesNo
                           -- | This instruction is supported in all later processors,
                           -- but only in the "latter steppings of the processor",
                           -- which I think means 64-bit mode only.
                           , proc_start_lat_step :: Maybe YesNo
                           }
{-
, pri_opcd_test_f      :: Maybe NonEmptyString
                                 , pri_opcd_modif_f     :: Maybe ModifF
                                 , pri_opcd_def_f       :: Maybe DefF
                                 , pri_opcd_undef_f     :: Maybe NonEmptyString
                                 , pri_opcd_f_vals      :: Maybe NonEmptyString
                                 , pri_opcd_modif_f_fpu :: Maybe NonEmptyString
                                 , pri_opcd_def_f_fpu   :: Maybe NonEmptyString
                                 , pri_opcd_undef_f_fpu :: Maybe NonEmptyString
                                 , pri_opcd_f_vals_fpu  :: Maybe NonEmptyString
-}

-- | All of the ways flags may be affected or used by an instruction.
data InstFlags = InstFlags { inst_flag_o :: InstFlagInfo -- ^ The overflow flag
                           , inst_flag_d :: InstFlagInfo -- ^ The direction flag
                           , inst_flag_i :: InstFlagInfo -- ^ The interrupt enable flag (???)
                           , inst_flag_s :: InstFlagInfo -- ^ The sign flag
                           , inst_flag_z :: InstFlagInfo -- ^ The zero flag
                           , inst_flag_a :: InstFlagInfo -- ^ The adjust flag
                           , inst_flag_p :: InstFlagInfo -- ^ The parity flag
                           , inst_flag_c :: InstFlagInfo -- ^ The carry flag
                           }

data InstFlagInfo = InstFlagInfo -- | Whether or not an instruction reads a flag.
                                 { inst_flag_tested   :: YesNo
                                 -- | Whether or not an instruction writes a flag.
                                 , inst_flag_modified :: YesNo
                                 -- | Whether or not an instruction is defined with a flag.
                                 , inst_flag_defined  :: InstFlagStatus
                                 -- | What value to which a flag is always set, if applicable.
                                 , inst_flag_value    :: Maybe Bool
                                 }

-- | Whether or not an instruction is defined with a flag, or "complicated"
-- for instructions whose behaviors are outside the scope of the source reference xml.
data InstFlagStatus = FlagDefined | FlagUndefined | FlagComplicated

-- | The instruction extension group with which an opcode was released.
-- See http://ref.x86asm.net/index.html#column_iext for details.
data InstrExt = InstrExtSSE1 | InstrExtMMX | InstrExtSSE2  | InstrExtSSE3  | InstrExtSSSE3
              | InstrExtVMX  | InstrExtSMX | InstrExtSSE41 | InstrExtSSE42

data ModifF = ModifF { modif_f_content :: NonEmptyString
                     , modif_f_cond    :: Maybe YesNo
                     }

data DefF = DefF { def_f_content :: NonEmptyString
                 , def_f_cond    :: Maybe YesNo
                 }

data PriOpcdMod = PriOpcdModMem | PriOpcdModNoMem

-- | Whether an FPU instruction pushes or pops the register stack.
data FPUStack = FPUStack { fpu_push :: YesNo
                         , fpu_pop  :: FPUPopTimes
                         }

-- | How many times an FPU instruction pops from the register stack.
data FPUPopTimes = PopNone | PopOne | PopTwo

-- | See http://ref.x86asm.net/#column_flds
data InstrFields = FieldRegisterCode         -- * A register code, 0-7, is added to the primary opcode.
                 | FieldsS Bool              -- * Sign-extend bit.
                 | FieldsD Bool              -- * Direction bit.
                 | FieldsW Bool              -- * Operand size bit.
                 | FieldsSW Bool Bool        -- * Sign-extend bit and operand size bit.
                 | FieldsDW Bool Bool        -- * Direction bit and operand size bit.
                 | FieldsTTTN Bool Bool Bool -- * Condition bit field, used in conditional instructions.
                 | FieldsSR Bool Bool        -- * Segment register specifier (one of the four original SRs).
                 | FieldsSRE Bool Bool Bool  -- * Segment register specifier (one of any SRs).
                 | FieldsMF Bool Bool        -- * Memory format, x87 instrs coded with second FP instr format.

data DocumentationStatus =
  DocumentationStatus { doc_status   :: IntelDocumentationStatus
                      -- | A link to an instruction's documentation,
                      -- or the reference describing an undocumented instruction.
                      , doc_ref      :: Maybe URL
      -- wip
                      -- | It's not really clear why we have three fields for documentation state yet,
                      -- so I'm not sure if they're redundant or add further nuance.
                      -- More research required.
                      -- Is this instruction documented.
                      , doc_is_doc   :: YesNo
                      -- | Is this instruction undocumented.
                      , doc_is_undoc :: YesNo
                      }

data IntelDocumentationStatus
  -- | "D". Fully documented within the Intel manuals. This is the default when not specified.
  = IntelDocumented
  -- | "M". Documented "only marginally".
  | IntelMarginallyDocumented
  -- | "U". Undocumented at all. As far as we're concerned, undocumented doesn't equal invalid.
  | IntelUndocumented

-- | The ring level an instruction is valid from.
-- Depends ("F") means it depends on flags, and should be associated with a reference to that flag.
-- Defaults to 3.
data RingLevel = RingLevel0 | RingLevel3 | RingLevelDepends RingLevelReference

-- | A reference to the description of that flag, if applicable.
data RingLevelReference = PriOpcdRingRefCr4TSD | PriOpcdRingRefCr4PCE | PriOpcdRingRefRFLAGS_IOPL

-- | The mode of operation an instruction is valid on. Virtual-8086 mode is not taken into account.
-- The default value of this column is R. For 64-bit editions, E usually means the semantics are 64-bit specific.
data OperationMode = RealMode      -- ^ Works in Real Mode, Protected Mode, and Extended Mode (not considering SMM)
                   | ProtectedMode -- ^ Works in Protected Mode, and Extended Mode (not considering SMM)
                   | ExtendedMode  -- ^ Works in Extended (64-bit) Mode (not considering SMM)
                   | SMM           -- ^ Works in System Management Mode

data PriOpcdAttr = PriOpcdAttrSerial | PriOpcdAttrInvd | PriOpcdAttrAcc | PriOpcdAttrDelaysInt
                 | PriOpcdAttrUndef  | PriOpcdAttrNull | PriOpcdAttrNop | PriOpcdAttrDelaysIntCond

data SyntaxType = SyntaxType { syntaxTypeParts :: [SyntaxTypePart]
                             , syntaxType_mod  :: Maybe NonEmptyString
                             }

data SyntaxTypePart = SyntaxTypePart { syntaxType_mnem :: Mnem
                                     , syntaxType_operands :: AtMostThirtyTwoOf SrcDestTypes
                                     }

data MnemContent
          = -- | An instruction mnemonic.
            Mnemonic NonEmptyString
            -- | There is no mnemonic for this opcode.
          | NoMnem
            -- | This opcode is invalid (e.g. an instruction which is no longer valid in 64-bit mode).
          | MnemInvalid
            -- | An instruction which is officially undefined in the manaul.
          | MnemUndefined
            -- | The opcode is an integer NOP instruction, with a link to the source.
          | MnemNop URL
            -- | The prefix has no meaning (no operation).
          | MnemNull

data Mnem = Mnem { mnem_content :: MnemContent
                 , mnem_sug     :: Maybe YesNo
                 }

data SrcDestTypes = SrcDestTypes { src_type  :: Maybe OperandType
                                 , dest_type :: Maybe OperandType
                                 }

data OperandType = OperandType 

loadInstructions :: IO X86Reference
loadInstructions = parseInstructions <$> readFile referenceXmlPath

referenceXmlPath :: FilePath
referenceXmlPath = "deps/x86reference/x86reference.xml"

parseInstructions :: String -> X86Reference
parseInstructions = parseInstructions

someFunc :: IO ()
someFunc = putStrLn "test"
